package fr.afpa.bean;
import java.util.ArrayList;
import java.util.Date;

public class Agence {
	
	private int codeAgence;
	private String nom;
	private String adresse;
	private ArrayList <Client> listeClient;
	
	public Agence (int monCodeAgence, String monNom, String monAdresse) {
		codeAgence = monCodeAgence;
		nom = monNom;
		adresse = monAdresse;
		listeClient = new ArrayList();
	}

	

	public int getCodeAgence() {
		return codeAgence;
	}



	public void setCodeAgence(int codeAgence) {
		this.codeAgence = codeAgence;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getAdresse() {
		return adresse;
	}



	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}



	public ArrayList<Client> getListeClient() {
		return listeClient;
	}


	
	public void setListeClient(ArrayList<Client> listeClient, Client a) {
		listeClient.add(a);
	}
	
	
	
	

}
