package fr.afpa.bean;
import java.util.ArrayList;
import java.util.Date;

public class Client {
	
	private String idClient;
	private String nom;
	private String prenom;
	private Date dateNaissance;
	private String email;
	private boolean actif;
	private ArrayList <CompteBancaire> listeCompteBancaire;
	
	
	public Client (String monIdClient, String monNom, String monPrenom, Date maDateNaissance, String monEmail, boolean monActif) {
		idClient = monIdClient;
		nom = monNom;
		prenom = monPrenom;
		dateNaissance = maDateNaissance;
		email = monEmail;
		actif = monActif;
		listeCompteBancaire = new ArrayList();
	}


	public String getIdClient() {
		return idClient;
	}


	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public Date getDateNaissance() {
		return dateNaissance;
	}


	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public boolean isActif() {
		return actif;
	}


	public void setActif(boolean actif) {
		this.actif = actif;
	}


	public ArrayList<CompteBancaire> getListeCompteBancaire() {
		return listeCompteBancaire;
	}


	public void setListeCompteBancaire(ArrayList<CompteBancaire> listeCompteBancaire, CompteBancaire e) {
		listeCompteBancaire.add(e);
	}
	
	
	

}
