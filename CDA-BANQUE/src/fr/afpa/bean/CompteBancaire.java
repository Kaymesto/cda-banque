package fr.afpa.bean;

public class CompteBancaire {
	private String numCompte;
	private float solde;
	private boolean decouvert;
	
	public CompteBancaire (String monNumCompte, float monSolde, boolean monDecouvert) {
		numCompte = monNumCompte;
		solde = monSolde;
		decouvert = monDecouvert;
	}

	public String getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

	public float getSolde() {
		return solde;
	}

	public void setSolde(float solde) {
		this.solde = solde;
	}

	public boolean getDecouvert() {
		return decouvert;
	}

	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}
	
	
}
