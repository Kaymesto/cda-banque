package fr.afpa.view;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import fr.afpa.bean.Agence;
import fr.afpa.bean.Client;
import fr.afpa.bean.CompteBancaire;
import fr.afpa.control.Control;
import fr.afpa.model.GestionBDD;


public class Display {

	public static void Menu(){
 		Scanner in = new Scanner(System.in);
		String choix = "";
		
		
		while (choix.toUpperCase() != "Q") {
			System.out.println("Bienvenue dans la CDA BANQUE, veuillez s�lectionner votre choix.");
			System.out.println("---------------------------MENU---------------------------");
			System.out.println("A - Cr�er une agence.");
			System.out.println("B - Cr�er un client.");
			System.out.println("C - Cr�er un compte bancaire.");
			System.out.println("D - Rechercher un compte (num�ro de compte).");
			System.out.println("E - Rechercher un client(Nom, Num�ro de compte, identifiant de client).");
			System.out.println("F - Afficher la liste des comptes d'un client (Identifiant client)");
			System.out.println("G - Imprimer les infos client (identifiant client).");
			System.out.println("H - D�sactiver un client.");
			System.out.println("I - Supprimer un compte.");
			System.out.println("Q - Quitter l'application.");
			choix = in.nextLine();
			
			switch(choix.toUpperCase()) {
			case "A" : 	
			System.out.println("Entrez le nom de l'agence.");
			String nomAgence = in.nextLine();
			System.out.println("Entrez l'adresse de l'agence.");
			String adresseAgence = in.nextLine();
			Agence ag = new Agence (100, nomAgence, adresseAgence);		
						GestionBDD.CreationAgence(ag);break;
			
			
			case "B" :   	//public Client (String monIdClient, String monNom, String monPrenom, Date maDateNaissance, String monEmail, boolean monActif) {
				boolean verification = false;
				String identifiantClient = "";
			while (verification == false) {
				System.out.println("Entrez l'identifiant du client sous le format suivant : LLCCCCCC (L pour lettre et C pour chiffre.");
				identifiantClient = in.nextLine();
				verification = Control.verifidClient(identifiantClient);}
				System.out.println("Entrez le nom du client.");
				String nom = in.nextLine();
				System.out.println("Entrez le pr�nom du client.");
				String prenom = in.nextLine();
				verification = false;
				Date dateNaissance = null;
				while (verification == false) {
				System.out.println("Entrez la date de naissance dans le format suivant dd-MM-yyyy (d pour jour, M pour mois et y pour ann�e).");
				String date = in.nextLine();
				try {     dateNaissance =new SimpleDateFormat("dd-MM-yyyy").parse(date);  
				verification = true;}
				catch (Exception e) {
					System.out.println("Merci de v�rifier le format de la date de naissance.");
				}}
				System.out.println("Entrez l'email du client.");
				String email = in.nextLine();
				System.out.println("Entrez le code de l'Agence responsable de ce client.");
				int codeAgence = in.nextInt();
				
				Client c = new Client (identifiantClient,nom, prenom,dateNaissance, email, true  );
				GestionBDD.creationClient(c, codeAgence);
				
				break;
		
			
			case "C" :  		//public CompteBancaire (String monNumCompte, float monSolde, boolean monDecouvert) {
					System.out.println("Entrez le num�ro de l'agence responsable de ce compte bancaire.");
					int numAgence = in.nextInt();
					in.nextLine();
					System.out.println("Entrez l'identifiant du client propri�taire du compte.");
					String identiClient = in.nextLine();
					System.out.println("Entrez le solde du compte.");
					float solde = in.nextFloat();
					boolean decouvert = false;
					String str = "";
					while (str.equalsIgnoreCase("oui") == false && str.equalsIgnoreCase("non")) {
						System.out.println("Le client b�n�ficie-t-il d'un d�couvert ?");
						str = in.nextLine();
					}
					if (str.equalsIgnoreCase("oui")) {
					decouvert = true;}
				
					CompteBancaire cb = new CompteBancaire ("10000000000",solde,decouvert);
					GestionBDD.creationCompteBancaire(numAgence, identiClient, cb);
					in.nextLine();
				break;
			
			
			case "D" :  System.out.println("Entrez le num�ro de compte");
						Long numeroCompte = in.nextLong();
						GestionBDD.affichageCompte(numeroCompte);
						in.nextLine();
						break;
			
			
			case "E" : break;
			
			
			case "F" : System.out.println("Entrez l'identifiant du client dont vous souhaitez afficher les comptes.");
			 		   String idClient = in.nextLine();
				GestionBDD.affichagetoutCompte(idClient);
				break;

			
			case "G" : break;
			
			
			case "H" : System.out.println("Entrez l'identifiant du client � d�sactiver.");
					   idClient = in.nextLine();
						GestionBDD.desactivationCompteClient(idClient);
				break;

			
			case "I" : System.out.println("Entrer le num�ro de compte bancaire � supprimer.");
			 		   long numCompte = in.nextLong();
			 		   GestionBDD.suppressionCompteBancaire(numCompte);
			 		   in.nextLine();
			 		   break;


			
			case "Q" :System.out.println("Vous avez quitt� l'application. Au Revoir.");
						System.exit(0);

			}		
		}

	
	
	
	
	
	
	
	
	
	
	
	
	
	}

}

