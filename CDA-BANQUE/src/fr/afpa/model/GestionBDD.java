package fr.afpa.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Scanner;

import fr.afpa.bean.Agence;
import fr.afpa.bean.Client;
import fr.afpa.bean.CompteBancaire;

public class GestionBDD {

	/**
	 * Affichage de compte bancaire
	 * @param numeroCompte
	 */
	public static void affichageCompte(long numeroCompte) {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");
			// Requette � executer
			String strQuery = "SELECT a.numcompte,a.codeagence,a.idclient,a.solde,a.decouvert FROM comptebancaire a where numcompte= "+numeroCompte;
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);
			// Utilisation du ResultSet
			while (listeServices.next()) {

				System.out.println("Numero de compte  : " + listeServices.getLong(1));
				System.out.println("Code Agence       : " + listeServices.getInt(2));
				System.out.println("Id Client         : " + listeServices.getString(3));
				System.out.println("Solde             : " + listeServices.getLong(4));
				if (listeServices.getBoolean(5) == true) {
				System.out.println("D�couvert         : Oui.");}
				else {System.out.println("D�couvert         : Non.");}
				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}}
			
	
	}
		
	
	
	/**
	 * Afficher tous les comptes d'un client
	 * @param numeroCompte
	 */
	public static void affichagetoutCompte(String idClient) {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");
			// Requette � executer
			String strQuery = "SELECT a.* FROM comptebancaire a,client b where a.idclient= '"+idClient+"' and b.idclient = a.idclient and  b.actif = 'true'";
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);
			// Utilisation du ResultSet
			while (listeServices.next()) {

				System.out.println("Numero de compte  : " + listeServices.getLong(1));
				System.out.println("Code Agence       : " + listeServices.getInt(2));
				System.out.println("Id Client         : " + listeServices.getString(3));
				System.out.println("Solde             : " + listeServices.getLong(4));
				if (listeServices.getBoolean(5) == true) {
				System.out.println("D�couvert         : Oui.");}
				else {System.out.println("D�couvert         : Non.");}
				System.out.println("---------------------------------------------------------");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}}
			
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Proc�dure de suppression de compte bancaire.
	 * @param numCompte
	 */
	public static void suppressionCompteBancaire(long numCompte) {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");

			String strQuery = "delete FROM comptebancaire where numcompte = "+numCompte;
			stServices = conn.createStatement();
					stServices.execute(strQuery);
		

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
						
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			

		}
		
	}			
		
	
	/**
	 * D�sactivation de compte client
	 * @param idClient
	 */
	public static void desactivationCompteClient(String idClient) {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");

			String strQuery = "update client set actif = 'false' where idClient = '"+idClient+"'";
			stServices = conn.createStatement();
					stServices.execute(strQuery);
		

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
						
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			

		}
		
	}


	/**
	 * Cr�ation d'Agence
	 */
	public static void CreationAgence(Agence ag) {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");

			String strQuery = "insert into Agence (select nextval('seq_codeagence'),'"+ag.getNom()+"','"+ag.getAdresse()+"');";
			stServices = conn.createStatement();
					stServices.execute(strQuery);
		

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
						
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}

		
		
	}


	/**
	 * Cr�ation objet client et insertion dans la BDD
	 * @param c
	 */
	public static void creationClient(Client c, int codeAgence) {
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");
			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat =new SimpleDateFormat(pattern);
			String strQuery = "insert into Client values ('"+c.getIdClient()+"',"+codeAgence+",'"+c.getNom()+"','"+c.getPrenom()+"','"+simpleDateFormat.format(c.getDateNaissance())+"','"+c.getEmail()+"',true)";
			stServices = conn.createStatement();
			stServices.execute(strQuery);
		

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
						
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}


/**
 * Cr�ation compte Bancaire
 * @param numAgence
 * @param identiClient
 * @param cb
 */
	public static void creationCompteBancaire(int numAgence, String identiClient, CompteBancaire cb) {
		Scanner in = new Scanner(System.in);
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");
			String pattern = "yyyy-MM-dd";
			SimpleDateFormat simpleDateFormat =new SimpleDateFormat(pattern);
			String strQuery = "insert into CompteBancaire (select nextval('seq_numcompte'),"+numAgence+",'"+identiClient+"',"+cb.getSolde()+","+cb.getDecouvert()+")";
			stServices = conn.createStatement();
			stServices.execute(strQuery);
		

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			System.out.println("\nL'identifiant du client ou le num�ro d'agence est erron�. Veuillez r�essayer en v�rifiant ces derniers.\n\n");
		} finally {
			
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
						
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
	}
}