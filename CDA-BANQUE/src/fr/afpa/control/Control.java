package fr.afpa.control;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Control {
	
	
	/**
	 * V�rification taille du num�ro de compte (11) et convertible en long.
	 * @param numCompte
	 * @return
	 */
	public static boolean verifNumCompte (String numCompte) {
		boolean verif = true;
		
		try {
			Long.parseLong(numCompte);
			if (numCompte.length() != 11) {
				verif = false;
			}
		}
		catch (Exception e) {
			verif = false;
		}
	
		return verif;	
	}
	
	
	/**
	 * V�rification taille code agence
	 * @param a
	 * @return
	 */
	public static boolean verifCodeAgence(int a) {
		boolean verif = true;
			if (String.valueOf(a).length() != 3) {
				verif = false;
			}
	
		return verif;	
	}
	
	
	/**
	 * V�rification format composition et taille de l'id du client
	 * @param client
	 * @return
	 */
	
	public static boolean verifidClient (String client) {
		boolean verif = true;
		Pattern p = Pattern.compile("[A-Za-z]{2}[\\d]{6}");
		Matcher m = p.matcher(client);
		if(m.matches()) {
			verif = true;
						}
		else {verif = false;}
		
		
		return verif;
	}
	
	
	/**
	 * V�rification du nombre de compte, si inf�rieur � 3 alors true sinon false;
	 * @param idClient
	 * @return
	 */
	public static boolean verifNombreCompte(String idClient) {
		boolean verif = false;
		Connection conn = null;
		Statement stServices = null;
		ResultSet listeServices = null;
		int compteur=0;
		try { 
			String driverName = "org.postgresql.Driver";
			Class.forName(driverName);
			
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/CDA-BANQUE", "postgres", "ADMINS");
			// Requette � executer
			String strQuery = "SELECT count(numcompte) FROM comptebancaire where idclient = '"+idClient+"'";
			stServices = conn.createStatement();
			listeServices = stServices.executeQuery(strQuery);
			// Utilisation du ResultSet
			while (listeServices.next()) {
					compteur = listeServices.getInt("count");
			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			
			// Fermeture du RS
			if (listeServices != null)
				try {
					listeServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture du statement
			
			if (stServices != null)
				try {
					stServices.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			// Fermeture de la connection
			if (conn != null)
				try {

					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			

		}
		if (compteur <3) {
			verif = true;
		}
		
		return verif;
	}
	

}
